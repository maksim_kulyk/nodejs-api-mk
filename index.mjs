import util from 'util';
import * as utils from 'test-utils';

const params = { password: 'sdf234fdsf32cdsc' };

const runMePleasePromise = util.promisify(utils.runMePlease);

try {
    const result = await runMePleasePromise(params);
} catch (error) {
    console.log(error);
}
